FROM python:3-alpine

WORKDIR /app

COPY app/requirements.txt /app/
RUN pip install -r requirements.txt
COPY app /app

ENTRYPOINT ["python"]
#what we run in cmd
CMD ["app.py"]